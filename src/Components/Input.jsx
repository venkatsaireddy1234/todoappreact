import React, { Component } from "react";

class Input extends Component {
  state = {
    task: "",
  };

  handleChange = (e) => {
    this.setState({ task: e.target.value });
  };
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.onInput(this.state.task);
    this.setState({ task: "" });
  };
  render() {
    return (
      <div id="input" className={this.props.boxColor ? "box-light" : "box-dark"}>
        <form onSubmit={this.handleSubmit} autoComplete="off">
          <input type="text" name="" className="new-item" placeholder="Create a new todo..." value={this.state.task}
          onChange={this.handleChange}/>
        </form>

      </div>
    );
  }
}
export default Input;