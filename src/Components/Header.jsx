import React, { Component } from "react";
import sun from "../Images/icon-sun.svg";
import moon from "../Images/icon-moon.svg"

class Header extends Component {
  
  render() {
    
    const {onThemeChange}=this.props;
    return (
      <header className="header">TODO
        <img onClick={onThemeChange} src={this.props.image?sun:moon}
          alt="icon" id="theme-image"/>
      </header>
    );
  }
}

export default Header;
