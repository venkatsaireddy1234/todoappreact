import React, { Component } from "react";
import cross from "../Images/icon-cross.svg"
import check from "../Images/icon-check.svg";

class List extends Component {
  render() {
    const { tasks, onRemove, boxColor } = this.props;
    return (
      <div  className={boxColor? "box-light":"box-dark"} id="licontainer">
        <ul id="list-items">
          {tasks.map((task) => {
            return (
              <li key={task.id} className="list-item">
                <input type="checkbox" id={task.id} checked={task.complete} className="custom-checkbox"
                  onClick={() => this.props.onToggle(task.id)} onChange={(e) => {}} />
                <span className="Nike">
                  <img src={check} alt="" />
                </span>
                <label htmlFor={task.id}>{task.title}</label>
                <img src={cross} className="delete" id={task.id} alt="remove" onClick={() => onRemove(task.id)} />
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}
export default List;