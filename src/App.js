import React, { Component } from "react";
import { v4 as uuid } from "uuid";

import "./App.css";
import Header from "./Components/Header";
import Input from "./Components/Input";
import List from "./Components/List";
import Footer from "./Components/Footer";


class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tasks: [], theme:true, toRender:"All"
    }
  }
 
   
  handleThemeChange = () => {
    this.setState({
      theme:!this.state.theme,  
    })
  };


  
  handleInput= (taskInput) => {
    if (taskInput !== "") {
      let task = {
        title: taskInput,
        id: uuid(),
        complete: false,
      };
      let tasks = [...this.state.tasks, task];
      this.setState({
        tasks
      });
    }
  };


  handleToggle = (toggleId) => {
    this.setState({
      tasks: this.state.tasks.map((task) => {
        if (task.id === toggleId) {
          return {
            ...task,
            complete: !task.complete,
          };
        }
        return task;
      }),
    });
  };



  handleRemove = (taskId) => {
    let tasks = this.state.tasks.filter((task) => task.id !== taskId);
    this.setState({ tasks });
  };

  handleClearComplete = () => {
    let tasks = this.state.tasks.filter((task) => task.complete === false);
    this.setState({ tasks });
  };

  handleFilterChange = (filter) => {
    this.setState({ toRender: filter.label });
  };

  
  handleDisplay = (filterTodisplay) => {
    if (filterTodisplay === "All") {
      return this.state.tasks;
    } else if (filterTodisplay === "Completed") {
      return this.state.tasks.filter((task) => task.complete === true);
    } else if (filterTodisplay === "Active") {
      return this.state.tasks.filter((task) => task.complete === false);
    } else {
      return [];
    }
  };


  render() {
    const taskToRender = this.handleDisplay(this.state.toRender);
    const tasksRemainig = this.state.tasks.filter((task) => task.complete === false).length;  
    return (
      <React.Fragment>
        <div className={this.state.theme ?  "lightBackground":"darkBackground" }>
          <div className="wrapper">
            <div className="container">
              {console.log(this.state.theme)}
              <Header onThemeChange={this.handleThemeChange} image={this.state.theme}/>
              <Input onInput={this.handleInput} boxColor={this.state.theme} />
              <List boxColor={this.state.theme} tasks={taskToRender} onToggle={this.handleToggle}
                onRemove={this.handleRemove}/>
              <Footer boxColor={this.state.theme} count={tasksRemainig} onFilterChange={this.handleFilterChange}
                onClearComplete={this.handleClearComplete}
              /> 
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default App;
